/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usuarios;

import interfaz.*;

/**
 *
 * @author Brank
 */
public class Administrador extends Persona {

    protected static Administrador usuarioActivo;
    
    public Administrador(String nombreCompleto, String correo, String contraseña) {
        super(nombreCompleto, correo, contraseña);
    }

    public static void miCuenta(Administrador admin) {
        usuarioActivo = (Administrador)interfaz.Sistema.usuarioActivo;
        boolean salir = false;
        while (!salir) {
            Util.limpiarPantalla();            
            System.out.println("================================================================================");
            System.out.println();            
            System.out.println("1. Nombre completo\n2. Correo\n3. Contraseña\n4. Atrás");
            System.out.println();
            System.out.println("================================================================================");
            System.out.print("Ingrese opción : ");
            String datoAEditar = Util.ingresoString();
            while ((!(Util.isNumeric(datoAEditar))) || (!(Util.isBetween(1, 4, datoAEditar)))) {
                System.out.print("Opción incorrecta. Ingrese nuevamente : ");
                datoAEditar = Util.ingresoString();
            }
            switch (datoAEditar) {
                case "1":
                    System.out.println("Nombre actual : " + usuarioActivo.getNombreCompleto());
                    System.out.print("Ingrese el nuevo nombre : ");
                    String nuevoNombre = Util.toTitle(Util.ingresoString());
                    admin.setNombreCompleto(nuevoNombre);
                    System.out.println();
                    System.out.println("Nombre modificado con éxito");
                    Util.continuar();
                    break;
                case "2":
                    System.out.println("Correo actual : " + usuarioActivo.getCorreo());
                    System.out.print("Ingrese el nuevo correo : ");
                    String nuevoCorreo = Util.ingresoString().toLowerCase();
                    admin.setCorreo(nuevoCorreo);
                    System.out.println();
                    System.out.println("Correo modificado con éxito");
                    Util.continuar();
                    break;
                case "3":
                    System.out.print("Ingrese su antigua contraseña : ");
                    String antiguaContraseña = Util.ingresoString();
                    if (antiguaContraseña.equals(admin.getContraseña())) {
                        System.out.print("Ingrese la nueva contraseña : ");
                        String nuevaContraseña = Util.ingresoString();
                        admin.setContraseña(nuevaContraseña);
                        System.out.println();
                        System.out.println("Contraseña modificada con éxito");
                        Util.continuar();
                        break;
                    } else {
                        System.out.println();
                        System.out.println("Contraseña incorrecta");
                        Util.continuar();
                        break;
                    }
                case "4":
                    salir = true;
                    break;
            }
        }
    }
}
