/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz;

import entidades.*;
import java.util.*;
import usuarios.*;

/**
 *
 * @author Brank
 */
public class Data {

    public static ArrayList<Empresa> empresas = new ArrayList<>();
    public static ArrayList<Persona> usuarios = new ArrayList<>();
    public static ArrayList<Tarjeta> tarjetas = new ArrayList<>();

    public static void mostrarEmpresas() {
        for (Empresa e : empresas) {
            System.out.println();
            System.out.println(e);
            for (Establecimiento est : e.getEstablecimientos()) {
                System.out.println();
                System.out.println(est);
                for (Promocion p : est.getPromociones()) {
                    System.out.println();
                    System.out.println(p);
                }
            }
            System.out.println();
            System.out.println("*-----------------------------------------------------------------------------*");            
        }
    }

    public static void mostrarPromociones() {
        ArrayList<Promocion> promociones = new ArrayList<>();
        for (Empresa e : empresas) {            
            for (Establecimiento est : e.getEstablecimientos()) {                
                for (Promocion p : est.getPromociones()) {
                    if (!(promociones.contains(p))) {
                        promociones.add(p);
                    }
                }
            }
        }
        for (Promocion promo : promociones) {
            System.out.println();
            System.out.println("Empresa : " + promo.getEstablecimiento().getEmpresa().getNombre());
            System.out.println("Establecimiento : "+ promo.getEstablecimiento().getDireccion());            
            System.out.println(promo);
            System.out.println();
            System.out.println("*-----------------------------------------------------------------------------*");
        }        
    }
}
